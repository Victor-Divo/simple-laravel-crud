# Instalasi

Clone repository

`git clone https://gitlab.com/Victor-Divo/simple-laravel-crud.git`

Masuk ke folder yang baru dibuat `simple-laravel-crud`

`cd simple-laravel-crud`

Duplicate File `.env.example` di Folder `simple-laravel-crud` dan ubah menjadi `.env` <br>
Kemudian Isi atau Edit File `.env` dengan Database Credential yang diperlukan

`APP_URL=http://simple-laravel-crud.test`

`DB_DATABASE=database_name`

Install the composer dependencies

`composer install`

Set application key

`php artisan key:generate`

Buat Database baru dengan nama `simple-laravel-crud` <br>
(atau samakan dengan nama yang dibuat di `APP_URL` dan `DB_DATABASE` tadi)

Import File Sql ini agar tidak perlu membuat tabel dan record lagi <br>
[SQL File Download](https://drive.google.com/file/d/1L6uHSXBFmm0h1xqq026fuhE2BwuTtlZg/view?usp=sharing)

# Penggunaan

Pastikan Web Server dan MySql Server sudah berjalan <br>
Jika mempunyai laragon yang sudah terinstal aplikasi dapat dibuka dengan `app-link.test` <br>

Jika tidak mempunyai laragon, jalankan perintah di terminal

`php artisan serve`

Halaman CRUD dapat diakses pada<br>

`http://simple-laravel-crud.test/diklat`
<br>
atau

`http://127.0.0.1:8000/diklat`
