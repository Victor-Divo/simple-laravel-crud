<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/diklat', 'DiklatController@index')->name('diklat');
Route::prefix('diklat')->name('diklat.')->group(function () {
    Route::get('/create', 'DiklatController@create')->name('create');
    Route::get('/{diklat}/edit', 'DiklatController@edit')->name('edit');
    Route::post('/delete/{diklat}', 'DiklatController@delete')->name('delete');
    Route::post('', 'DiklatController@store')->name('store');
    Route::post('/update/{diklat}', 'DiklatController@update')->name('update');
});
