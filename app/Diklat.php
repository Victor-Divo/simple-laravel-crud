<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Diklat extends Model
{
    protected $table = 'diklat';

    const CREATED_AT = 'createdAt';
    const UPDATED_AT = 'updatedAt';

    protected $fillable = [
        'nama', 'jenis', 'penyelenggara',
        'jumlahPeserta', 'tanggalMulai', 'tanggalSelesai',
        'waktuMulai', 'waktuSelesai', 'durasi',
        'tempat', 'sertifikat', 'biaya',
        'kadaluarsaSertifikat', 'statusKadaluarsaSertifikat', 'deskripsi',
        'createdBy', 'updatedBy'
    ];

    public function getTanggalMulaiAttribute($tanggalMulai)
    {
        return Carbon::parse($tanggalMulai)->format('Y-m-d');
    }

    public function getTanggalSelesaiAttribute($tanggalSelesai)
    {
        return Carbon::parse($tanggalSelesai)->format('Y-m-d');
    }

    public function getKadaluarsaSertifikatAttribute($tanggalSelesai)
    {
        return Carbon::parse($tanggalSelesai)->format('Y-m-d');
    }

    public function setBiayaAttribute($biaya)
    {
        return $biaya ?? 0;
    }

    public function setWaktuMulaiAttribute($waktuMulai)
    {
        return $waktuMulai ?? '';
    }

    public function setWaktuSelesaiAttribute($waktuSelesai)
    {
        return $waktuSelesai ?? '';
    }

    public function setDeskripsiAttribute($deskripsi)
    {
        return $deskripsi ?? '';
    }
}
