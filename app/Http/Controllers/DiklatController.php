<?php

namespace App\Http\Controllers;

use App\Diklat;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class DiklatController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Diklat::query()
            ->select('id', 'nama', 'jenis', 'penyelenggara', 'jumlahPeserta', 'tanggalMulai', 'tanggalSelesai', 'durasi', 'tempat')
            ->latest();

            return DataTables::eloquent($data)
            ->addColumn('aksi', function($d) {
                $edit = route('diklat.edit', $d);
                return
                '<button  class="btn btn-sm btn-outline-primary my-1">Detail</button>
                <a href="'. $edit .'" class="btn btn-sm btn-outline-warning my-1">Edit</a>
                <button class="btn btn-sm btn-outline-danger my-1" onclick="showModal('.$d->id.')">Delete</button>';
            })
            ->editColumn('tanggalMulai', function ($d) {
                return Carbon::parse($d->tanggalMulai)->format('Y-m-d');
            })
            ->editColumn('tanggalSelesai', function ($d) {
                return Carbon::parse($d->tanggalSelesai)->format('Y-m-d');
            })
            ->rawColumns(['aksi'])
            ->addIndexColumn()
            ->toJson();
        }

        return view('diklat.index');
    }

    public function create()
    {
        return view('diklat.create');
    }

    public function store(Request $request)
    {
        $validatedData = $this->validateData($request);
        $mergedData = array_merge($validatedData, ['createdBy' => 1, 'updatedBy' => 1]);

        $diklat =  Diklat::create($mergedData);
        if ($request->statusKadaluarsaSertifikat == 1) {
            $diklat->statusKadaluarsaSertifikat = 1;
        } elseif ($request->kadaluarsaSertifikat) {
            $diklat->kadaluarsaSertifikat = $request->kadaluarsaSertifikat;
        }
        $diklat->save();
        session()->flash('message', 'Diklat Berhasil Ditambahkan');
        return redirect()->route('diklat');

    }

    public function edit(Diklat $diklat)
    {
        return view('diklat.edit', compact('diklat'));
    }

    public function update(Request $request, Diklat $diklat)
    {
        $validatedData = $this->validateData($request);

        $diklat->update($validatedData);
        if ($request->statusKadaluarsaSertifikat == 1) {
            $diklat->statusKadaluarsaSertifikat = 1;
        } elseif ($request->kadaluarsaSertifikat) {
            $diklat->kadaluarsaSertifikat = $request->kadaluarsaSertifikat;
        }
        $diklat->save();
        session()->flash('message', 'Diklat Berhasil Diedit');
        return redirect()->route('diklat');
    }

    private function validateData($data)
    {
        return $data->validate([
            'nama' => 'required',
            'jenis' => 'required',
            'deskripsi' => 'nullable|string',
            'penyelenggara' => 'required',
            'biaya' => 'nullable|numeric',
            'jumlahPeserta' => 'required',
            'kadaluarsaSertifikat' => 'nullable|date',
            'sertifikat' => 'required',
            'tanggalMulai' => 'required',
            'tanggalSelesai' => 'required',
            'waktuMulai' => 'nullable|string|date_format:H:i',
            'waktuSelesai' => 'nullable|string|date_format:H:i',
            'durasi' => 'required',
            'tempat' => 'nullable|string|max:255',
        ]);
    }

    public function delete(Diklat $diklat)
    {
        $diklat->delete();
        session()->flash('message', 'Data Berhasil Dihapus');
        return redirect()->back();
    }
}
