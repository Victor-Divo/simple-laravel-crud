@extends('layout.app')
@section('title', 'Daftar Diklat')
@section('page', 'Daftar Diklat')

@push('styles')
    {{-- Datatable CSS --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.0.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap5.min.css">
@endpush
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <a href="{{route('diklat.create')}}" class="btn btn-primary mb-5">Tambah Diklat</a>
                    <div class="table-responsive">
                        <table id="daftarDiklat" class="table table-bordered text-center" style="width: 100%">
                            <thead>
                              <tr>
                                <th scope="col">No</th>
                                <th scope="col">Nama Diklat</th>
                                <th scope="col">Jenis</th>
                                <th scope="col">Penyelenggara</th>
                                <th scope="col">Jumlah Peserta</th>
                                <th scope="col">Tanggal Mulai</th>
                                <th scope="col">Tanggal Berakhir</th>
                                <th scope="col">Durasi</th>
                                <th scope="col">Tempat</th>
                                <th scope="col">Aksi</th>
                              </tr>
                            </thead>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Delete Modal -->
    <div class="modal fade" id="deleteModal" tabindex="-1" aria-labelledby="deleteModalLabel" aria-hidden="true">
        <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="deleteModalLabel">Konfirmasi</h5>
            <button type="button" class="btn-close" onclick="toggleModal()" aria-label="Close"></button>
            </div>
            <div class="modal-body">
            Apakah anda yakin ingin menghapus data?
            </div>
            <div class="modal-footer">
                <form action="" id="modalDeleteForm" method="POST">
                    @csrf
                    <button type="submit" id="modalDeleteButton" class="btn btn-primary">Ya</button>
                </form>
                <button type="button" class="btn btn-secondary" onclick="toggleModal()">Tidak</button>
            </div>
        </div>
        </div>
    </div>
@endsection
@push('scripts')
    {{-- Datatable CDN --}}
    <script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap5.min.js"></script>
    {{-- Sweetalert2 CDN --}}
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>


    <script>
        // Datatable diklat
        $(document).ready(function() {
        $('#daftarDiklat').DataTable( {
            "ajax": '{{route('diklat')}}',
            "columns": [
                { "data": "DT_RowIndex" },
                { "data": "nama" },
                { "data": "jenis" },
                { "data": "penyelenggara" },
                { "data": "jumlahPeserta" },
                { "data": "tanggalMulai" },
                { "data": "tanggalSelesai" },
                { "data": "durasi" },
                { "data": "tempat" },
                { "data": "aksi", orderable: false, searchable: false, width: '20rem'},
            ]
        } );

    } );

    // Set delete form action in Delete Modal
    const deleteModal = new bootstrap.Modal(document.getElementById('deleteModal'))
    const toggleModal = () => deleteModal.toggle();
    function showModal (id) {
        toggleModal();
        const url = "{{route('diklat.delete', '')}}";
        $('#modalDeleteForm').attr('action', `${url}/${id}`);
    }
    </script>

    @if (session()->has('message'))
        <script>
            $(document).ready(function(){
                Swal.fire({
                    icon: 'success',
                    title: "{{session('message')}}",
                });
            });
        </script>
    @endif
@endpush
