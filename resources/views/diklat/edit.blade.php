@extends('layout.app')
@section('title', 'Edit Diklat')
@section('page', 'Edit Diklat')

@section('breadcumb')
    <li class="nav-item">
        <a class="nav-link disabled">></a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{route('diklat')}}">Diklat</a>
    </li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <form method="POST" action="{{action('DiklatController@update', $diklat)}}">
                        @csrf
                        <div class="row">
                            <div class="form-group col-sm-6">
                                <h3>Informasi Diklat</h3>
                                <div class="mb-3">
                                    <label for="nama">Nama Diklat <span class="text-danger"> *</span></label>
                                    <input name="nama" type="text" class="form-control" id="nama" placeholder="Nama Diklat" required value="{{$diklat->nama}}">
                                </div>
                                <div class="mb-3">
                                    <label for="jenis">Jenis </label><span class="text-danger"> *</span>
                                    <select name="jenis" class="select2bs4 custom-select" id="jenis" required>
                                        <option value="Internal">Internal</option>
                                        <option value="Eksternal">Eksternal</option>
                                    </select>
                                </div>
                                <div class="mb-3">
                                    <label for="deskripsi">Deskripsi</label>
                                    <input name="deskripsi" type="text" class="form-control" placeholder="Deskripsi" id="deskripsi" value="{{$diklat->deskripsi ?? ''}}">
                                </div>
                                <div class="mb-3">
                                    <label for="penyelenggara">Penyelenggara </label><span class="text-danger"> *</span>
                                    <input name="penyelenggara" type="text" class="form-control" placeholder="Penyelenggara" id="penyelenggara" required value="{{$diklat->penyelenggara}}">
                                </div>
                                <div class="mb-3">
                                    <label for="biaya">Biaya</label>
                                    <input name="biaya" type="number" class="form-control" id="biaya" placeholder="Biaya" value="{{$diklat->biaya}}">
                                </div>
                                <div class="mb-3">
                                    <label for="jumlahPeserta">Jumlah Peserta </label><span class="text-danger"> *</span>
                                    <input name="jumlahPeserta" type="number" class="form-control" id="jumlahPeserta" placeholder="Jumlah Peserta" required value="{{$diklat->jumlahPeserta}}">
                                </div>
                                <div class="mb-3">
                                    <label for="kadaluarsaSertifikat">Tanggal Kadaluarsa Sertifikat</label>
                                    <div class="row">
                                        <div class="col-sm-7 pr-0">
                                            <input name="kadaluarsaSertifikat" type="date" class="form-control" id="kadaluarsaSertifikat" placeholder="Tanggal Kadaluarsa Sertifikat" value="{{$diklat->kadaluarsaSertifikat}}">
                                        </div>
                                        <div class="col-sm">
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="checkbox" name="statusKadaluarsaSertifikat" id="statusKadaluarsaSertifikat" value="1" value="{{$diklat->statusKadaluarsaSertifikat}}">
                                                <label class="form-check-label" for="statusKadaluarsaSertifikat">Berlaku Selamanya</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="mb-3">
                                    <label for="sertifikat">Sertifikat </label><span class="text-danger"> *</span>
                                    <select name="sertifikat" class="select2bs4 custom-select" id="sertifikat" required>
                                        <option value="0">Tidak Ada</option>
                                        <option value="1">Ada</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col">
                                <h3>Waktu dan Tempat</h3>
                                <div class="mb-3">
                                    <label for="tanggalMulai">Tanggal Diklat </label><span class="text-danger"> *</span>
                                    <input name="tanggalMulai" type="date" class="form-control" placeholder="Tanggal Diklat" id="tanggalMulai" required value="{{$diklat->tanggalMulai}}">
                                </div>
                                <div class="mb-3">
                                    <label for="tanggalSelesai">Tanggal Selesai </label><span class="text-danger"> *</span>
                                    <input name="tanggalSelesai" type="date" class="form-control" placeholder="Tanggal Selesai" id="tanggalSelesai" required value="{{$diklat->tanggalSelesai}}">
                                </div>
                                <div class="mb-3">
                                    <label for="waktuMulai">Waktu Mulai</label>
                                    <div class="row">
                                        <div class="col-sm-5">
                                            <input name="waktuMulai" type="time" class="form-control" id="waktuMulai" value="{{$diklat->waktuMulai}}">
                                        </div>
                                        <div class="col-sm-2">
                                            <label class="form-label my-1" for="waktuSelesai">Sampai</label>
                                        </div>
                                        <div class="col-sm">
                                            <input name="waktuSelesai" type="time" class="form-control" id="waktuSelesai" value="{{$diklat->waktuSelesai}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="mb-3">
                                    <label for="durasi">Durasi </label><span class="text-danger"> *</span>
                                    <input name="durasi" type="number" class="form-control" id="durasi" placeholder="Durasi" required value="{{$diklat->durasi}}">
                                </div>
                                <div class="mb-3">
                                    <label for="tempat">Lokasi</label>
                                    <input name="tempat" type="text" maxlength="255" class="form-control" placeholder="Lokasi" id="tempat" value="{{$diklat->tempat}}">
                                </div>
                                <div class="float-right mt-3">
                                    <button class="btn btn-primary" type="submit">Simpan Perubahan</button>
                                    <a href="{{route('diklat')}}" class="btn btn-secondary">Batal</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    {{-- Sweetalert2 CDN --}}
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <script>
        $('input[name="statusKadaluarsaSertifikat"]').change(function() {
            $('#kadaluarsaSertifikat').attr('disabled', $('input[name="statusKadaluarsaSertifikat"]:checked').length == 1);
        });
    </script>
    @if ($errors->any())
        <script>
            $(document).ready(function(){
                const error = '{{implode(" | ", $errors->all())}}';
                Swal.fire({
                    icon: "error",
                    title: "Error",
                    text: error,
                });
            });
        </script>
    @endif
@endpush
